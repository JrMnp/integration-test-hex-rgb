// TDD - Unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("Hex to RGB conversion", () => {
        it("converts the basic colors", () => {
            const redRgb = converter.HexToRgb("ff0000"); // red 255,0,0
            const greenRgb = converter.HexToRgb("00ff00"); // green 0,255,0
            const blueRgb = converter.HexToRgb("0000ff"); // blue 0,0,255

            expect(redRgb).to.deep.equal([255,0,0]);
            expect(greenRgb).to.deep.equal([0,255,0]);
            expect(blueRgb).to.deep.equal([0,0,255]);
        });
    });
});