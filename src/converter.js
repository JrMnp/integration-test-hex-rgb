/**
 * Padding outputs 2 chars always
*@param {string} Rgb one or two char
*@returns {string} hex with two char
*/

module.exports = {
    /**
     * @param {number} red
     * @param {number} green
     * @param {number} blue
     * @returns {string}
     */

    HexToRgb: (hex) => {
        //console.log({hex});
        const redRgb = parseInt(hex.substring(0, 2), 16); // 0-255 -> 0-ff
        const greenRgb = parseInt(hex.substring(2, 4), 16);
        const blueRgb = parseInt(hex.substring(4, 6), 16);
        //const rgb = pad(redRgb) + pad(greenRgb) + pad(blueRgb);
        //console.log({hex});
        return [redRgb, greenRgb, blueRgb]; // hex string 6 characters
    }
}